# -*- mode: Shell-script -*-
# termcap terminfo
# ks      smkx      make the keypad send commands
# ke      rmkx      make the keypad send digits
# vb      flash     emit visual bell
# mb      blink     start blink
# md      bold      start bold
# me      sgr0      turn off bold, blink and underline
# so      smso      start standout (reverse video)
# se      rmso      stop standout
# us      smul      start underline
# ue      rmul      stop underline
#

export LESS_TERMCAP_mb=$(tput setaf 4)
export LESS_TERMCAP_md=$(tput bold ; tput setaf 4)
export LESS_TERMCAP_me=$(tput sgr0)
export LESS_TERMCAP_se=$(tput sgr0)
export LESS_TERMCAP_so=$(tput bold ; tput setaf 1)
export LESS_TERMCAP_ue=$(tput sgr0)
export LESS_TERMCAP_us=$(tput setaf 6)

export HISTCONTROL=ignoreboth
export PYENV_ROOT=${PYENV_ROOT:-"$HOME/.pyenv"}
export PYENV_VIRTUALENV_DISABLE_PROMPT=1
export SYSTEMD_PAGER=cat

export GODIR=${GODIR:-~/go}
export GOPATH=${GOPATH:-~/go}

if [[ -e /etc/bash_completion.d/git-prompt ]]; then
    . /etc/bash_completion.d/git-prompt
    GIT_PS1_SHOWCOLORHINTS=true
    GIT_PS1_SHOWDIRTYSTATE=true
    GIT_PS1_SHOWUNTRACKEDFILES=true
    GIT_PS1_SHOWSTASHSTATE=true
    GIT_PS1_SHOWUPSTREAM=auto
    GIT_PS1_DESCRIBE_STYLE=branch
    PROMPT_COMMAND='echo -ne "\033]0;${HOSTNAME}: ${PWD/$HOME/\~}\007" && __git_ps1 "[\t] \u@\h:\w${VIRTUAL_ENV:+ (${VIRTUAL_ENV##*/})}" "\$ "'
else
    PROMPT_COMMAND='echo -ne "\033]0;${HOSTNAME}: ${PWD/$HOME/\~}\007"'
fi

# Deal with SSH agent socket
if [ -S "$SSH_AUTH_SOCK" ] && [ ! -h "$SSH_AUTH_SOCK" ]; then
    ln -sf "$SSH_AUTH_SOCK" ~/.ssh/ssh_auth_sock
    export SSH_AUTH_SOCK=~/.ssh/ssh_auth_sock
fi

# Debstuff
export DEBFULLNAME=${DEBFULLNAME:-"Marko Crnić"}
export DEBEMAIL=${DEBEMAIL:-"macrnic@gmail.com"}

# PATH manipulations
export PATH="/usr/lib/ccache:$PYENV_ROOT/bin:$PATH:~/.bin:$GOPATH/bin:~/.local/bin"
export MANPATH="${MANPATH}:${HOME}/.local/share/man"

if [ -x ~/.pyenv/libexec/pyenv ]; then
    eval "$(pyenv init -)"
    test -x ~/.pyenv/plugins/pyenv-virtualenv/bin/pyenv-virtualenv-init && eval "$(pyenv virtualenv-init -)"
fi
