" Bootstrap vim-plug
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -sfLo ~/.vim/autoload/plug.vim --create-dirs
      \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

" Load plugs
call plug#begin('~/.vim/plugged')
Plug 'altercation/vim-colors-solarized'
Plug 'tpope/vim-surround'
Plug 'vim-utils/vim-man'
call plug#end()

" Keybinds
let mapleader = "\<Space>"
nmap <leader>0 :wincmd c<cr>
nmap <leader>3 :vsplit<cr>
nmap <leader>2 :split<cr>
nmap <leader>b :ls<cr>
nmap <leader>f :e
nmap <leader>wo :wincmd w<cr>
nmap <C-X>o :wincmd w<cr>

" Colors
syn on
let g:solarized_termcolors=256
set background=light
colorscheme solarized

highlight ExtraWhitespace ctermbg=red guibg=red
au BufEnter * match ExtraWhitespace /\s\+$/

" Options
set autoindent
set backspace=indent,eol,start
set encoding=utf-8
set expandtab
set formatoptions=tcqrnj
set hlsearch
set ignorecase
set incsearch
set laststatus=2
set nocompatible
set relativenumber
set shiftwidth=4
set smartindent
set smarttab
set softtabstop=4
set statusline=%<\ %n:%f\ %m%r%y%=%-35.(line:\ %l\ of\ %L,\ col:\ %c%V\ (%P)%)
set tabstop=4
set viminfo='50,<1000,s100,%
set wildignorecase
set wildmenu
set wildmode=list:full

" GUI
if has("gui_running")
    set mouse=v
    set guioptions=agirL
else
    set mouse=a
endif

" Load local settings
if !empty(glob('~/.vim/local.vim'))
    source ~/.vim/local.vim
endif
