export ALTERNATE_EDITOR="vim"
export EDITOR="emacsclient -t"
export VISUAL="emacsclient -c"
alias vim="emacsclient -t"
alias gvim="emacsclient -nc"
alias e='emacsclient -a "" -t'
alias ge='emacsclient -a "" -nc'
alias dps='docker ps --format "{{.ID}}\t{{.Status}}\t{{.Image}}\t{{.Names}}"'
alias k='kubectl'

alias ccat='highlight $1 --out-format xterm256 --force -s solarized-light'
